(function(global){
  global.Optilink = global.Optilink || {};

  $.extend(global.Optilink, {
    CreateTemp: CreateTemp
  });

  function CreateTemp($root, data){
    var father_div = null;
    var that   = {};
    var opt_data = data || [];
    var search_data = opt_data || [];
    var data_id = '';
    var _rootDiv = 
      '<div class="divcss1"></div>';

    function _createTemp(opt_i ,text){
      var _temp = '<div class="temp_div" id="'+opt_i +'">'+text+'</div>';
      return _temp;
    };

    function _create(data){
      $("div.divcss1").empty();
      for(var i=0;i<data.length;i++){
        var _temp = _createTemp(i, data[i].text);
        $('div.divcss1').append(_temp);
      }
    };

    function _init(){
      $root.after(_rootDiv);
      _create(search_data);
      data_id = '';

      $root.on('click', function(event){
        $("div.divcss1").show();
        _stopEventBuble(event);
      });

      $("div.divcss1").on('click', '.temp_div', _fillData);

      $(document).on('click', function(event){
        var $target = $(event.target);
        $("div.divcss1").hide();
      })
    };

    function _getClickData(){
      var click_data = '';
      if(data_id){
        click_data = search_data[data_id];
      }else{
        click_data = false;
      }
      return click_data
    }

    function _fillData(event){
      var fill_data = event.target.innerText;
      data_id = event.target.id;
      $root.val(fill_data);
    }

    function _filterData(text){
      var _search_data = [];
      for(var i=0;i<opt_data.length;i++){
        if(opt_data[i].text.indexOf(text) > -1){
          _search_data.push(opt_data[i])
        };
      };
      search_data = _search_data; 
    }

    $root.on('keyup',function(){//keyup键盘输入结束时间
      var _text = $root.val();
      if(_text){
        _filterData(_text);
      }else{
        search_data = opt_data;
      }
       _create(search_data);
      
    });

    // 阻止冒泡
    function _stopEventBuble(ev) {
      if (ev && ev.stopPropagation) {
        ev.stopPropagation();//非IE
      } else {
        ev.cancelBubble = true;//IE
      }
    }
    _init();
    that.getClickData = _getClickData;
    return that;
  }

  
})(window)