(function(global){
  global.Optilink = global.Optilink || {};

  $.extend(global.Optilink, {
    CargoesTable: CargoesTable
  });

  function CargoesTable($root){
    var $table = null;
    var that   = {};
    var _wrapper = 
    '<table border="1" cellspacing="0" class="transfer_cargoes" style="">' +
    '<thead>' +
    '<tr>' +
    '<th width="80px;"><span style="color: #40FF00">*</span>货物名</th>' +
    '<th width="80px;"><span style="color: #40FF00">*</span>包装</th>' +
    '<th width="80px;"><span style="color: #40FF00">*</span>件数</th>' +
    '<th width="80px;"><span style="color: #40FF00">*</span>尺寸(cm)</th>' +
    '<th width="80px;"><span style="color: #40FF00">*</span>重量(kg)</th>' +
    '<th width="80px;">操作</th>' +
    '</tr>' +
    '</thead>' +
    '<tbody></tbody>' +
    '</table>' +
    '<div style="padding-left: 464px;">' +
    '<button class="addtr" type="button" style="">增加</button>' +
    '</div>';

    var _tableBody = 
      '<tr>' +
      '<td><input type="text" name="cargo_name" value=""  style="width: 80px;"></td>' +
      '<td>' +
        '<select name="pack" style="width: 80px">' +
          '<option value="nude">裸装</option>' +
          '<option value="box">箱子</option>' +
          '<option value="bag">袋子</option>' +  
        '</select>' + 
      '</td>' +
      '<td><input type="text" name="sum" value="" style="width: 80px"></td>' +
      '<td><input type="text" name="size" value="" style="width: 80px"></td>' +
      '<td><input type="text" name="weight" value="" style="width: 80px"></td>' +
      '<td><a class="delete" href="javascript:void (0);">删除</a></td>' +
      '</tr>';

    function _create(){
      $root.html(_wrapper);
      $table = $root.find("table"); 
    }

    function _bindingEvent(){
      $root.on('click', 'button.addtr', _addTr);
      $root.on('click', 'a.delete', _deleteTr);
    }

    function _addTr(){
      $('table.transfer_cargoes').find('tbody').append(_tableBody);
    }

    function _deleteTr(){
      var $_td = $(this).parent();
      var $_tr = $_td.parent();
      $_tr.remove();
    }

    function _getInfo(){
      var cargoes = [];
      var $_tbody = $table.find("tbody");
      
      $_tbody.find('tr').each(function(){
        var $_tr = $(this);
        var cargoe = {};
        cargoe['cargo_name'] = $_tr.find('input[name="cargo_name"]').val();
        cargoe['pack'] = $_tr.find('select[name="pack"]').val();
        cargoe['sum'] = $_tr.find('input[name="sum"]').val();
        cargoe['size'] = $_tr.find('input[name="size"]').val();
        cargoe['weight'] = $_tr.find('input[name="weight"]').val();
        cargoes.push(cargoe);
      });
      return cargoes;
    }

    that.create = _create;
    that.getInfo = _getInfo;

    _bindingEvent();
    return that;

  }
})(window)